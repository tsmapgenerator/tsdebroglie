﻿using DeBroglie.Console.Config;
using DeBroglie.Console.Export;
using DeBroglie.Models;
using DeBroglie.Topo;
using System.IO;
using TSMap;
using System;
using System.Collections.Generic;

namespace DeBroglie.Console.Export
{

    public class TSMapExporter : IExporter
    {
        private string ParseFixedNeighbor(string currentTile, string newFixedTile, string neighborTile)
        {
            int current_index = Int32.Parse(currentTile.Split('_')[0]);
            int current_subindex = Int32.Parse(currentTile.Split('_')[1]);
            int current_level = Int32.Parse(currentTile.Split('_')[2]);
            int current_overlay = Int32.Parse(currentTile.Split('_')[3]);
            int current_overlay_data = Int32.Parse(currentTile.Split('_')[4]);

            string n_index = neighborTile.Split('_')[0];
            string n_subindex = neighborTile.Split('_')[1];
            string n_level = neighborTile.Split('_')[2];
            string n_overlay = neighborTile.Split('_')[3];
            string n_overlay_data = neighborTile.Split('_')[4];

            string f_index = newFixedTile.Split('_')[0];
            string f_subindex = newFixedTile.Split('_')[1];
            string f_level = newFixedTile.Split('_')[2];
            string f_overlay = newFixedTile.Split('_')[3];
            string f_overlay_data = newFixedTile.Split('_')[4];

            if (f_overlay == "x")
            {
                f_overlay = "" + n_overlay;
            }
            if (f_overlay_data == "x")
            {
                f_overlay_data = "" + n_overlay_data;
            }

            if (neighborTile.Split(';').Length > 1)
            {
                return f_index + "_" + f_subindex + "_" + f_level + "_" + f_overlay + "_" + f_overlay_data + ";" + neighborTile.Split(';')[1];
            }
            else
            {
                return f_index + "_" + f_subindex + "_" + f_level + "_" + f_overlay + "_" + f_overlay_data;
            }
        }

        public void Export(TileModel model, TilePropagator tilePropagator, string filename, DeBroglieConfig config, ExportOptions exportOptions)
        {
            var tiledExportOptions = exportOptions as TSMapExportOptions;
            if (tiledExportOptions == null)
            {
                throw new System.Exception($"Cannot export from {exportOptions.TypeDescription} to .mpr");
            }

            var map = tiledExportOptions.Template;
            var srcFilename = tiledExportOptions.SrcFileName;

            var layerArray = tilePropagator.ToArray();
            var processedArray = new string[layerArray.Topology.Width, layerArray.Topology.Height];
            int Full_Width = layerArray.Topology.Width;
            int Full_Height = layerArray.Topology.Height;
            map.Map_Width = layerArray.Topology.Width / 2;
            map.Map_Height = layerArray.Topology.Height / 2;

            var XPlusMap = new Dictionary<string, string>();
            var YPlusMap = new Dictionary<string, string>();
            foreach (var td in config.Tiles)
            {
                if (td.Xplus != null)
                {
                    XPlusMap[td.Value] = td.Xplus;
                }
                if (td.Yplus != null)
                {
                    YPlusMap[td.Value] = td.Yplus;
                }
            }

            for (ushort y = 0; y < Full_Height; y++)
            {
                for (ushort x = 0; x < Full_Width; x++)
                {
                    processedArray[x, y] = layerArray.Get(x, y).ToString();
                    // Check for null
                    if (processedArray[x, y] is null || processedArray[x, y].ToString() is null || processedArray[x, y].ToString() == "null")
                    {
                        System.Console.WriteLine("Found contradiction! Replacing with clear tile...");
                        processedArray[x, y] = "0_0_0_255_0";
                    }
                }
            }

            for (ushort y = 0; y < Full_Height; y++)
            {
                for (ushort x = 0; x < Full_Width; x++)
                {
                    string t = layerArray.Get(x, y).ToString();
                    if (XPlusMap.ContainsKey(t) && x + 1 < Full_Width)
                    {
                        string nt = layerArray.Get(x + 1, y).ToString();
                        processedArray[x + 1, y] = ParseFixedNeighbor(t, XPlusMap[t], nt);
                    }
                    if (YPlusMap.ContainsKey(t) && y + 1 < Full_Height)
                    {
                        string nt = layerArray.Get(x, y + 1).ToString();
                        processedArray[x, y + 1] = ParseFixedNeighbor(t, YPlusMap[t], nt);
                    }
                }
            }

            // Clear temp tiles
            for (ushort y = 0; y < Full_Height; y++)
            {
                for (ushort x = 0; x < Full_Width; x++)
                {
                    string t = processedArray[x, y].ToString();
                    int current_index = Int32.Parse(t.Split('_')[0]);
                    int current_level = Int32.Parse(t.Split('_')[2]);
                    if (current_index >= 65500)
                    {
                        processedArray[x, y] = "0_0_" + (current_level).ToString() + "_255_0";
                    }
                }
            }

            map.TileSetToMapPack(processedArray, (uint)layerArray.Topology.Width, (uint)layerArray.Topology.Height, (uint)layerArray.Topology.IndexCount);
            map.OverlaySetToOverlayPack(processedArray, (uint)layerArray.Topology.Width, (uint)layerArray.Topology.Height);
            map.WaypointsToSection(processedArray, (uint)layerArray.Topology.Width, (uint)layerArray.Topology.Height);
            map.TerrainToSection(processedArray, (uint)layerArray.Topology.Width, (uint)layerArray.Topology.Height);

            using (System.IO.StreamWriter file =
                new System.IO.StreamWriter(@"output_isotiles.txt"))
            {
                for (ushort y = 0; y < Full_Height; y++)
                {
                    for (ushort x = 0; x < Full_Width; x++)
                    {
                        if (layerArray.Get(x, y) == null)
                        {
                            file.Write("" + "".ToString().PadLeft(5, '0') + "_" + "".ToString().PadLeft(2, '0') + "_" + "".ToString().PadLeft(2, '0') + " ");
                        }
                        else
                        {
                            string parsedStr = processedArray[x, y].Split(';')[0].Split('|')[0];
                            file.Write("" + parsedStr.Split('_')[0].PadLeft(5, '0') + "_" + parsedStr.Split('_')[1].PadLeft(2, '0') + "_" + parsedStr.Split('_')[2].PadLeft(2, '0') + " ");
                        }
                    }
                    file.WriteLine();
                }
            }

            using (System.IO.StreamWriter file2 =
                new System.IO.StreamWriter(@"output_overlay.txt"))
            {
                for (ushort y = 0; y < Full_Height; y++)
                {
                    for (ushort x = 0; x < Full_Width; x++)
                    {
                        if (layerArray.Get(x, y) == null)
                        {
                            file2.Write("" + "".ToString().PadLeft(3, '0') + "_" + "".ToString().PadLeft(3, '0') + " ");
                        }
                        else
                        {
                            string parsedStr = processedArray[x, y].Split(';')[0].Split('|')[0];
                            file2.Write("" + parsedStr.Split('_')[3].PadLeft(3, '0') + "_" + parsedStr.Split('_')[4].PadLeft(3, '0') + " ");
                        }
                    }
                    file2.WriteLine();
                }
            }

            using (System.IO.StreamWriter file2 =
                new System.IO.StreamWriter(@"output_waypoints.txt"))
            {
                for (ushort y = 0; y < Full_Height; y++)
                {
                    for (ushort x = 0; x < Full_Width; x++)
                    {
                        if (layerArray.Get(x, y) == null || layerArray.Get(x, y).ToString().Split(';').Length < 2)
                        {
                            file2.Write("" + "".ToString().PadLeft(2, '0') + " ");
                        }
                        else
                        {
                            string parsedStr = processedArray[x, y].Split(';')[1].Split('|')[0];
                            file2.Write("" + parsedStr.PadLeft(2, '0') + " ");
                        }
                    }
                    file2.WriteLine();
                }
            }

            using (System.IO.StreamWriter file2 =
                new System.IO.StreamWriter(@"output_terrain.txt"))
            {
                for (ushort y = 0; y < Full_Height; y++)
                {
                    for (ushort x = 0; x < Full_Width; x++)
                    {
                        if (layerArray.Get(x, y) == null || layerArray.Get(x, y).ToString().Split('|').Length < 2)
                        {
                            file2.Write("" + "--------" + " ");
                        }
                        else
                        {
                            string TileStr = layerArray.Get(x, y).ToString();
                            file2.Write("" + TileStr.Split('|')[1] + " ");
                        }
                    }
                    file2.WriteLine();
                }
            }

            map.Save(filename);
        }
    }
}
