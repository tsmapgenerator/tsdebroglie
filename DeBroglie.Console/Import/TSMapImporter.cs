﻿using DeBroglie.Console.Export;
using DeBroglie.Tiled;
using DeBroglie.Topo;
using TSMap;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DeBroglie.Console.Import
{
    public class TSMapImporter : ISampleSetImporter
    {
        public SampleSet Load(string filename)
        {
            string srcFileName = filename;
            TSMap.TSMap map = new TSMap.TSMap(srcFileName);
            ITopoArray<Tile> sample;
            MapTileContainer[,] sample_tiles = map.GetTiles();
            OverlayContainer[,] overlay = map.GetOverlay();
            MapTileContainer[,] tiles = new MapTileContainer[2 * map.Map_Width, 2 * map.Map_Height];
            WaypointContainer[,] waypoints = map.GetWaypoints();
            TerrainObjectContainer[,] terrain = map.GetTerrain();

            var tilesByName = new Dictionary<string, Tile>();
            Tile[,] results = new Tile[2*map.Map_Width, 2*map.Map_Height];
            GridTopology topology = new GridTopology(2*map.Map_Width, 2*map.Map_Height, false);

            for (ushort y = 0; y < 2 * map.Map_Height; y++)
            {
                for (ushort x = 0; x < 2 * map.Map_Width; x++)
                {
                    if (sample_tiles[x, y] != null)
                    {
                        tiles[x, y] = new MapTileContainer(sample_tiles[x, y].X, sample_tiles[x, y].Y, sample_tiles[x, y].TileIndex, sample_tiles[x, y].SubTileIndex, sample_tiles[x, y].Level, sample_tiles[x, y].IceGrowth);
                    }
                }
            }

            for (ushort y = 0; y < 2 * map.Map_Height; y++)
            {
                for (ushort x = 0; x < 2 * map.Map_Width; x++)
                {
                    int tileindex = 0;
                    int subtileindex = 0;
                    int level = 0;
                    byte overlayindex = 0xFF;
                    byte overlaydata = 0;
                    string tilename;
                    string overlayname;
                    string waypointname = "-1";
                    string terrainname = "";

                    if (tiles[x, y] != null)
                    {
                        if (tiles[x, y].TileIndex < 0 || tiles[x, y].TileIndex >= 65535)
                        {
                            tileindex = 0;
                        }
                        else
                        {
                            tileindex = tiles[x, y].TileIndex;
                        }

                        subtileindex = tiles[x, y].SubTileIndex;
                        level = tiles[x, y].Level;

                    }

                    if (overlay[x, y] != null)
                    {
                        if (overlay[x, y].Index < 0 || overlay[x, y].Index >= 255)
                        {
                            overlayindex = 0xFF;
                        }
                        else
                        {
                            overlayindex = (byte)overlay[x, y].Index;
                        }
                        overlaydata = (byte)overlay[x, y].Value;
                    }

                    if (waypoints[x, y] != null)
                    {
                        waypointname = waypoints[x, y].Waypoint.ToString();
                    }

                    if (terrain[x, y] != null)
                    {
                        terrainname = terrain[x, y].Object.ToString();
                    }

                    tilename = tileindex.ToString() + "_" + subtileindex.ToString() + "_" + level.ToString();
                    overlayname = overlayindex + "_" + overlaydata;
                    results[x, y] = new Tile(tilename + "_" + overlayname);
                    if (waypointname != "-1")
                    {
                        results[x, y] = new Tile(results[x, y].ToString() + ";" + waypointname);
                    }
                    if (terrainname != "")
                    {
                        results[x, y] = new Tile(results[x, y].ToString() + "|" + terrainname);
                    }
                }
            }

            using (System.IO.StreamWriter file =
                new System.IO.StreamWriter(@"input_isotiles.txt"))
            {
                for (ushort y = 0; y < 2 * map.Map_Height; y++)
                {
                    for (ushort x = 0; x < 2 * map.Map_Width; x++)
                    {
                        if (tiles[x, y] == null)
                        {
                            file.Write("" + "".ToString().PadLeft(5, '0') + "_" + "".ToString().PadLeft(2, '0') + "_" + "".ToString().PadLeft(2, '0') + " ");
                        }
                        else
                        {
                            file.Write("" + tiles[x, y].TileIndex.ToString().PadLeft(5, '0') + "_" + tiles[x, y].SubTileIndex.ToString().PadLeft(2, '0') + "_" + tiles[x, y].Level.ToString().PadLeft(2, '0') + " ");
                        }
                    }
                    file.WriteLine();
                }
            }

            using (System.IO.StreamWriter file =
                new System.IO.StreamWriter(@"input_overlay.txt"))
            {
                for (ushort y = 0; y < 2 * map.Map_Height; y++)
                {
                    for (ushort x = 0; x < 2 * map.Map_Width; x++)
                    {
                        if (overlay[x, y] == null)
                        {
                            file.Write("" + "255".ToString().PadLeft(3, '0') + "_" + "".ToString().PadLeft(3, '0') + " ");
                        }
                        else
                        {
                            file.Write("" + overlay[x, y].Index.ToString().PadLeft(3, '0') + "_" + overlay[x, y].Value.ToString().PadLeft(3, '0') + " ");
                        }
                    }
                    file.WriteLine();
                }
            }

            using (System.IO.StreamWriter file =
                new System.IO.StreamWriter(@"input_waypoints.txt"))
            {
                for (ushort y = 0; y < 2 * map.Map_Height; y++)
                {
                    for (ushort x = 0; x < 2 * map.Map_Width; x++)
                    {
                        if (waypoints[x, y] == null)
                        {
                            file.Write("" + "-1".ToString().PadLeft(2, '0') + " ");
                        }
                        else
                        {
                            file.Write("" + waypoints[x, y].Waypoint.ToString().PadLeft(2, '0') + " ");
                        }
                    }
                    file.WriteLine();
                }
            }

            using (System.IO.StreamWriter file =
                new System.IO.StreamWriter(@"input_terrain.txt"))
            {
                for (ushort y = 0; y < 2 * map.Map_Height; y++)
                {
                    for (ushort x = 0; x < 2 * map.Map_Width; x++)
                    {
                        if (terrain[x, y] == null)
                        {
                            file.Write("" + "-------- ");
                        }
                        else
                        {
                            file.Write("" + terrain[x, y].Object + " ");
                        }
                    }
                    file.WriteLine();
                }
            }

            sample = TopoArray.Create(results, topology);

            return new SampleSet
            {
                Directions = sample.Topology.AsGridTopology().Directions,
                Samples = new[] { sample },
                TilesByName = tilesByName,
                ExportOptions = new TSMapExportOptions
                {
                    Template = map,
                    SrcFileName = srcFileName,
                },
            };
        }

        public Tile Parse(string tile)
        {
            return new Tile(tile);
        }
    }
}
