﻿using System.Collections.Generic;
using System.Linq;
using System;

namespace DeBroglie.Constraints
{
    public class FixedTileCircleConstraint : ITileConstraint
    {
        public Tile[] Tiles { get; set; }

        public Point? Center { get; set; }

        public float Radius { get; set; }

        public void Check(TilePropagator propagator)
        {
        }

        public void Init(TilePropagator propagator)
        {
            var tileSet = propagator.CreateTileSet(Tiles);

            var point = Center ?? GetRandomPoint(propagator, tileSet);

            SelectRandomCircle(point.X, point.Y, point.Z, propagator, tileSet);
        }

        private void SelectRandomCircle(int X, int Y, int Z, TilePropagator propagator, TilePropagatorTileSet tileset)
        {
            int fixed_X = Math.Max(Convert.ToInt32(Radius) + 1, X);
            int fixed_Y = Math.Max(Convert.ToInt32(Radius) + 1, Y);
            int start_x = fixed_X - Convert.ToInt32(Radius);
            int start_y = fixed_Y - Convert.ToInt32(Radius);
            int end_x = fixed_X + Convert.ToInt32(Radius);
            int end_y = fixed_Y + Convert.ToInt32(Radius);
            for (int i = start_x; i < end_x; ++i)
            {
                for (int j = start_y; j < end_y; ++j)
                {
                    if (Math.Sqrt(Math.Pow(i - fixed_X, 2) + Math.Pow(j - fixed_Y, 2)) <= Radius)
                    {
                        propagator.Select(i, j, Z, tileset);
                    }
                }
            }
        }

        public Point GetRandomPoint(TilePropagator propagator, TilePropagatorTileSet tileSet)
        {
            var topology = propagator.Topology;

            var points = new List<Point>();
            for (var z = 0; z < topology.Depth; z++)
            {
                for (var y = 0; y < topology.Height; y++)
                {
                    for (var x = 0; x < topology.Width; x++)
                    {
                        if (topology.Mask != null)
                        {
                            var index = topology.GetIndex(x, y, z);
                            if (!topology.Mask[index])
                                continue;
                        }

                        propagator.GetBannedSelected(x, y, z, tileSet, out var isBanned, out var _);
                        if (isBanned)
                            continue;

                        points.Add(new Point(x, y, z));
                    }
                }
            }

            // Choose a random point to select
            if (points.Count == 0)
                throw new System.Exception($"No legal placement of {tileSet}");

            var i = (int)(propagator.RandomDouble() * points.Count);

            return points[i];

        }
    }
}
