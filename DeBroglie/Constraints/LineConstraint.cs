﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DeBroglie.Constraints
{
    /// <summary>
    /// LineConstraint class 
    /// </summary>
    public class LineConstraint : ITileConstraint
    {
        /// <summary>
        /// The tiles to select or ban fromthe  border area.
        /// </summary>
        public Tile[] Tiles { get; set; }

        public Point Start { get; set; }
        public Point End { get; set; }

        public void Check(TilePropagator propagator)
        {
        }

        public void Init(TilePropagator propagator)
        {
            var tiles = propagator.CreateTileSet(Tiles);

            int dx = Math.Abs(End.X - Start.X);
            int dy = -Math.Abs(End.Y - Start.Y);
            int sx = Start.X < End.X ? 1 : -1;
            int sy = Start.Y < End.Y ? 1 : -1;
            int err = dx + dy;
            int e2;

            int x = Start.X;
            int y = Start.Y;

            while(true)
            {
                propagator.Select(x, y, Start.Z, tiles);
                if (x == End.X && y == End.Y) { break; }
                e2 = 2 * err;
                if (e2 >= dy) { err += dy; x += sx; }
                if (e2 <= dx) { err += dx; y += sy; }
            }
        }
    }
}
