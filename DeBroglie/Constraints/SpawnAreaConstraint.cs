﻿using System.Collections.Generic;
using System.Linq;
using System;

namespace DeBroglie.Constraints
{
    public class SpawnAreaConstraint : ITileConstraint
    {
        public Tile[] TiberiumTiles { get; set; }
        public Tile[] TiberiumTreeTiles { get; set; }
        public Tile[] ClearTiles { get; set; }

        public Point? SpawnPoint { get; set; }
        public Tile WaypointTile { get; set; }

        public float? TibCircleMiddleRadMultX;
        public float? TibCircleMiddleRadMultY;
        public float? TibTreeRadMultX;
        public float? TibTreeRadMultY;

        public float Radius { get; set; }

        public void Check(TilePropagator propagator)
        {
        }

        public void Init(TilePropagator propagator)
        {
            var tiberiumTileSet = propagator.CreateTileSet(TiberiumTiles);
            var clearTileSet = propagator.CreateTileSet(ClearTiles);
            var tiberiumTreeTileSet = propagator.CreateTileSet(TiberiumTreeTiles);

            var spawnPoint = SpawnPoint ?? GetRandomPoint(propagator, clearTileSet);
            var point2 = spawnPoint;
            point2.X -= Convert.ToInt32(Radius * (TibCircleMiddleRadMultX ?? 0.75));
            point2.Y -= Convert.ToInt32(Radius * (TibCircleMiddleRadMultY ?? 0.75));
            var point3 = spawnPoint;
            point3.X -= Convert.ToInt32(Radius * (TibTreeRadMultX ?? 0.88));
            point3.Y -= Convert.ToInt32(Radius * (TibTreeRadMultY ?? 0.88));

            SelectSpawnArea(spawnPoint, point2, point3, propagator, tiberiumTileSet, clearTileSet, tiberiumTreeTileSet);
        }

        private void SelectSpawnArea(Point spawnPoint, Point point2, Point point3, TilePropagator propagator, TilePropagatorTileSet tiberiumTileSet, TilePropagatorTileSet clearTileSet, TilePropagatorTileSet tiberiumTreeTileSet)
        {
            float radius2 = Radius * 0.5f;
            int start_x = Math.Max(0, spawnPoint.X - Convert.ToInt32(Radius));
            int start_y = Math.Max(0, spawnPoint.Y - Convert.ToInt32(Radius));
            int end_x = spawnPoint.X + Convert.ToInt32(Radius);
            int end_y = spawnPoint.Y + Convert.ToInt32(Radius);
            int start_x2 = Math.Max(0, point2.X - Convert.ToInt32(radius2));
            int start_y2 = Math.Max(0, point2.Y - Convert.ToInt32(radius2));
            int end_x2 = point2.X + Convert.ToInt32(radius2);
            int end_y2 = point2.Y + Convert.ToInt32(radius2);
            for (int i = Math.Min(start_x, start_x2); i < Math.Max(end_x, end_x2); ++i)
            {
                for (int j = Math.Min(start_y, start_y2); j < Math.Max(end_y, end_y2); ++j)
                {
                    if (Math.Sqrt(Math.Pow(i - spawnPoint.X, 2) + Math.Pow(j - spawnPoint.Y, 2)) <= Radius)
                    {
                        if (i == spawnPoint.X && j == spawnPoint.Y)
                        {
                            propagator.Select(i, j, spawnPoint.Z, WaypointTile);
                        }
                        else
                        {
                            propagator.Select(i, j, spawnPoint.Z, clearTileSet);
                        }
                    }
                    else if (Math.Sqrt(Math.Pow(i - point2.X, 2) + Math.Pow(j - point2.Y, 2)) <= radius2)
                    {
                        if (i == point3.X && j == point3.Y)
                        {
                            propagator.Select(i, j, point3.Z, tiberiumTreeTileSet);
                        }
                        else
                        {
                            propagator.Select(i, j, point2.Z, tiberiumTileSet);
                        }
                    }
                }
            }
        }

        public Point GetRandomPoint(TilePropagator propagator, TilePropagatorTileSet tileSet)
        {
            var topology = propagator.Topology;

            var points = new List<Point>();
            for (var z = 0; z < topology.Depth; z++)
            {
                for (var y = 0; y < topology.Height; y++)
                {
                    for (var x = 0; x < topology.Width; x++)
                    {
                        if (topology.Mask != null)
                        {
                            var index = topology.GetIndex(x, y, z);
                            if (!topology.Mask[index])
                                continue;
                        }

                        propagator.GetBannedSelected(x, y, z, tileSet, out var isBanned, out var _);
                        if (isBanned)
                            continue;

                        points.Add(new Point(x, y, z));
                    }
                }
            }

            // Choose a random point to select
            if (points.Count == 0)
                throw new System.Exception($"No legal placement of {tileSet}");

            var i = (int)(propagator.RandomDouble() * points.Count);

            return points[i];

        }
    }
}
